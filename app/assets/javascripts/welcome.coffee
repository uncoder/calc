$ ->
  $('#calculator .clear').click ->
    $('#calculator .screen').text ''

  $('#calculator .keys span').not('.eval').click ->
    $('#calculator .screen').append $(@).text()

  $('#calculator .keys .eval').click ->
    $.post window.calc_path, { exp: $('#calculator .screen').text() }
class WelcomeController < ApplicationController

  def index
  end

  def calculate
    calc = Calculator.new params[:exp]
    @result = calc.calculate
  end

end

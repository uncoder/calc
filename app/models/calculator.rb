class Calculator
  @@ops = {
    1 => { '*' => :mult, '/' => :div },
    0 => { '+' => :add, '-' => :sub }
  }

  def initialize str
    @str = str
  end

  def calculate
    return 'err' unless valid?

    arr = @str.split /([+\-*\/])/
    1.downto(0) do |pr|
      arr.each_with_index do |val, i|
        if @@ops[pr].keys.include? val
          arr[i+1] = send @@ops[pr][val], arr[i-1].to_f, arr[i+1].to_f
          arr[i-1], arr[i] = nil
        end
      end
      arr.reject! { |c| c.nil? }
    end
    res = arr.first
    res = res.to_i if res == res.to_i
    return res
  end

  private

  def valid?
    not (@str =~ /^\s*-?(\d+|[0-9]*[.][0-9]+)(?:\s*[-+*\/]\s*(\d+|[0-9]*[.][0-9]+))+$/).nil?
  end

  def mult a, b
    a * b
  end

  def div a, b
    a / b
  end

  def add a, b
    a + b
  end

  def sub a, b
    a - b
  end
end
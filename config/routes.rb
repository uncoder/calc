Rails.application.routes.draw do
  
  root 'welcome#index'
  post '/calculate', to: 'welcome#calculate', as: :calculate

end
